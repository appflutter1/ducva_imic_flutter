import 'dart:math';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Buổi 2 Part 2',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Buổi 2 part 2'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: [
            Row(children: [
              Column(
                children: [
                  const Text('Giới thiệu ảnh'),
                ],
              ),
              Column(
                children: [
                  Image.network(
                    'https://scontent.fhan7-1.fna.fbcdn.net/v/t1.6435-9/83145832_1186333848239970_6883962410818863104_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=0debeb&_nc_ohc=zbpjqQQGwwMAX-xH0o3&_nc_ht=scontent.fhan7-1.fna&oh=00_AT8r5aSl01NgpzmO_KzTrXzqNNT9Si1b6MEQpUzb_rsA0Q&oe=637FC360',
                    width: 200,
                  )
                ],
              ),
            ]),
            Row(children: [
              Column(
                children: [
                  const Text('Giới thiệu ảnh'),
                ],
              ),
              Column(
                children: [
                  Image.network(
                    'https://scontent.fhan7-1.fna.fbcdn.net/v/t1.6435-9/83145832_1186333848239970_6883962410818863104_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=0debeb&_nc_ohc=zbpjqQQGwwMAX-xH0o3&_nc_ht=scontent.fhan7-1.fna&oh=00_AT8r5aSl01NgpzmO_KzTrXzqNNT9Si1b6MEQpUzb_rsA0Q&oe=637FC360',
                    width: 200,
                  )
                ],
              ),
            ]),
          ],
        ),
      ),
    );
  }
}

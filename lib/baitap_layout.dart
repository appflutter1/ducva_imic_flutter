// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, sort_child_properties_last

import 'dart:math';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Screen Login',
      theme: new ThemeData(scaffoldBackgroundColor: const Color(0xFFEFEFEF)),
      //theme: ThemeData(
      //primarySwatch: Colors.blue,
      //),
      home: const MyHomePage(title: ''),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(children: [
          Container(
            child: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Login ',
                      textScaleFactor: 2,
                      style: TextStyle(color: Colors.white),
                    ),
                    Text(
                      'Hi, welcome back',
                      textScaleFactor: 2,
                      style: TextStyle(color: Colors.white, fontSize: 8),
                    )
                  ]),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30)),
              color: Color.fromRGBO(105, 89, 208, 0.4),
              boxShadow: [
                // BoxShadow(
                //     color: Color.fromARGB(255, 192, 97, 236), spreadRadius: 3),
              ],
            ),
            height: 80,
          ),
          Container(
            //padding: const EdgeInsets.fromLTRB(20, 10, 20, 2),
            margin: EdgeInsets.fromLTRB(30, 30, 30, 2),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    contentPadding: EdgeInsets.all(20),
                  ),
                ),
                SizedBox(
                  height: 10, // khoảng cách giữa 2 textfield
                ),
                TextField(
                    decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                  contentPadding: EdgeInsets.all(20),
                )),
              ],
            ),
          ),
          Container(

              // padding: const EdgeInsets.only(left: 2.0),
              // margin: EdgeInsets.only(left: 5),
              child: Column(
            children: [
              Wrap(
                children: <Widget>[
                  Checkbox(
                    checkColor: Colors.blue[400],
                    activeColor: Colors.white,
                    // side: BorderSide.strokeAlignCenter,
                    // fillColor: Colors.blue,
                    value: isChecked,
                    onChanged: (bool? value) {
                      setState(() {
                        isChecked = value!;
                      });
                    },
                  ),
                  Text(
                    'Remember me',
                    style: TextStyle(fontSize: 13.0),
                  ),
                  SizedBox(
                    width: 200, // khoảng cách giữa 2 text
                  ),
                  Text(
                    'Forgot password?',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ],
              ),
            ],
          )),
          Container(
            margin: EdgeInsets.fromLTRB(30, 10, 30, 5),
            child: Wrap(
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Color.fromRGBO(105, 89, 208, 0.6),
                      minimumSize: const Size.fromHeight(
                          50), // fix cho chiều dài button bằng với container
                      shape: StadiumBorder()), // border radius cho button
                  onPressed: () {},
                  child: const Text(
                    'Login',
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Raleway',
                      fontSize: 22.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(30, 15, 30, 5),
            child: Wrap(
              children: [
                Text('------------------'),
                Text(' or log in with '),
                Text('------------------'),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(30, 10, 30, 5),
            child: Wrap(children: [
              ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Color.fromARGB(255, 255, 255, 255),
                  shape: StadiumBorder(),
                  onPrimary: Colors.black,
                ),
                onPressed: () {},
                icon: Icon(
                    Icons.g_mobiledata_rounded), //icon data for elevated button
                label: Text("Google"),
              ),
              ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(60, 90, 154, 1),
                    shape: StadiumBorder()),
                onPressed: () {},
                icon: Icon(
                    Icons.facebook_outlined), //icon data for elevated button
                label: Text("Facebook"), //label text
              ),
            ]),
          )
        ]),
      ),
    );
  }
}
